FROM ubuntu:20.04
LABEL Description="Docker image for Oddmuse wiki"

RUN apt-get update && apt-get install -y \
    libmojolicious-perl \
    libmojolicious-plugin-cgi-perl \
    libcgi-pm-perl \
    libxml-rss-perl \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /app

COPY server.pl /app
COPY wiki.pl /app

VOLUME ["/app/wiki"]

EXPOSE 3000
WORKDIR /app
ENV WikiDataDir="/app/wiki"

CMD ["perl", "server.pl", "daemon"]
